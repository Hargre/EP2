/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author felipe
 */
public class LoginController {
	public static boolean authenticateLogin(String username, String password) {
		HashMap<String, String> loginInfo = new HashMap<>();
		String line;
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader("employeeInfo.txt"));
			while ((line = reader.readLine()) != null) {
				String[] parts = line.split(":", 2);
				if (parts.length >= 2) {
					String key = parts[0];
					String value = parts[1];
					loginInfo.put(key, value);
				}
			}
		} catch (FileNotFoundException ex) {
			Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		return (loginInfo.containsKey(username) && loginInfo.get(username).equals(password));
	}
}
