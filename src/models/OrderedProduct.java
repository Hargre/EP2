/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author felipe
 */
public class OrderedProduct {
	private final int amount;
	private double price;
	private final Product product;
	
	public OrderedProduct(Product product, int amount) {
		this.amount = amount;
		this.product = product;
		getProductFromStock();
	}
	
	public double getPrice() {
		return price;
	}
	
	private void getProductFromStock() {
		product.getItems(this.amount);
		this.price = product.getPrice() * this.amount;
	}
}
