/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author felipe
 */
public class CreditCardPayment implements Payment {
	private double valuePaid;
	
	public double getValuePaid() {
		return valuePaid;
	}
	
	@Override
	public void pay(Order order) {
		valuePaid = order.getValue();
	}
	
}
