/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author felipe
 */
public class Stock {
	private int amount;
	private final int minimumAmount;

	Stock(int amount) {
		this.minimumAmount = 5;
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public boolean isStockLow(int amountOrdered) {
		int stockControl = amount - amountOrdered;
		return stockControl <= minimumAmount;
	}
}
