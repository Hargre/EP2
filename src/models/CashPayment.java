/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author felipe
 */
public class CashPayment implements Payment {
	private final double cashGiven;
	private double change;
	
	public CashPayment(double cashGiven) {
		this.cashGiven = cashGiven;
	}
	
	public double getChange() {
		return change;
	}
	
	@Override
	public void pay(Order order) {
		change = cashGiven - order.getValue();
	}
	
}
