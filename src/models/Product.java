/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author felipe
 */
public class Product {
	private final String name;
	private final double price;
	private final Stock stock;
	
	public Product(String name, double price, int amount) {
		this.name = name;
		this.price = price;
		stock = new Stock(amount);
	}
	
	public String getName() {
		return name;
	}

	public double getPrice() {
		return price;
	}
	
	public Stock getStock() {
		return stock;
	}
	
	public void getItems(int amount) {
		int stockAfterOrder = this.stock.getAmount() - amount;
		this.stock.setAmount(stockAfterOrder);
	}
}
