/*
 * To change this license header, choose License Headers in Project Properties.
 * To chang	e this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.List;

/**
 *
 * @author felipe
 */
public class Order {
	Client client;
	List <OrderedProduct> products;
	String observation;
	double totalValue;
	
	public Order(String name, List <OrderedProduct> products, String observation) {
		this.client = new Client(name);
		this.products = products;
		this.observation = observation;
		this.totalValue = 0;
		setValue();
	}
	
	public double getValue() {
		return totalValue;
	}
	
	private void setValue() {
		for (OrderedProduct product : products) {
			totalValue += product.getPrice();
		}
	}
}
