/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author felipe
 */
public final class Register {
	private JFrame registerFrame;
	private JPanel headerPanel;
	private JPanel inputPanel;
	private JPanel buttonsPanel;
	private JTextField usernameText;
	private JPasswordField passwordText;
	private JButton confirmButton;
	private JButton cancelButton;
	
	public Register() {
		setWindow();
	}
	
	private void setWindow() {
		setFrame();
		setHeaderPanel();
		setInputPanel();
		setButtonsPanel();
		setConfirmButton();
		setCancelButton();
		showWindow();
	}

	private void setFrame() {
		registerFrame = new JFrame("Registration");
		registerFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void setHeaderPanel() {
		headerPanel = new JPanel();
		
		JLabel headerLabel = new JLabel("Registration");
		headerPanel.add(headerLabel);
		
		registerFrame.add(headerPanel);
	}
	
	private void setInputPanel() {
		inputPanel = new JPanel();
		inputPanel.setLayout(new GridLayout(2, 2));
		
		JLabel usernameLabel = new JLabel("Username");
		inputPanel.add(usernameLabel);
		usernameText = new JTextField(10);
		inputPanel.add(usernameText);
		
		JLabel passwordLabel = new JLabel("Password");
		inputPanel.add(passwordLabel);
		passwordText = new JPasswordField(10);
		inputPanel.add(passwordText);
		
		registerFrame.add(inputPanel);
	}

	private void setButtonsPanel() {
		buttonsPanel = new JPanel();
		registerFrame.add(buttonsPanel);
	}

	private void setConfirmButton() {
		confirmButton = new JButton("Confirm");
		confirmButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			
				try {
					FileWriter writer = new FileWriter("employeeInfo.txt", true);
					writer.write(System.lineSeparator());
					usernameText.write(writer);
					writer.write(":");
					passwordText.write(writer);
				} catch (IOException ex) {
					Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
				}
				
				registerFrame.dispose();
				Login login = new Login();
			}
		});
		
		buttonsPanel.add(confirmButton);
	}

	private void setCancelButton() {
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				registerFrame.dispose();
				Login login = new Login();
			}			
		});
		
		buttonsPanel.add(cancelButton);
	}

	private void showWindow() {
		registerFrame.pack();
		registerFrame.setLocationRelativeTo(null);
		registerFrame.setLayout(new FlowLayout());
		registerFrame.setSize(300, 160);
		registerFrame.setVisible(true);
	}

}
