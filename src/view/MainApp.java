/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import models.CashPayment;
import models.CreditCardPayment;
import models.Order;
import models.OrderedProduct;
import models.Product;

/**
 *
 * @author felipe
 */
public class MainApp {
	private JFrame mainFrame;
	private JTabbedPane tabs;
	private JPanel orderPanel;
	private JPanel stockPanel;
	private JScrollPane stockTablePane;
	private JTable stockTable;
	
	private final Product burger = new Product("Burger", 10.0, 50);
	private final Product lasagna = new Product("Lasagna", 20.0, 50);
	private final Product soda = new Product("Soda", 3.0, 50);
	private final Product coffee = new Product("Coffee (Capsule)", 2.0, 50);
	private final Product iceCream = new Product("Ice Cream", 5.0, 50);
	private final Product chocoCake = new Product("Chocolate Cake", 5.0, 50);
	
	public MainApp() {
		setWindow();
	}

	private void setWindow() {
		setFrame();
		setTabPane();
		setOrderPanel();
		setStockPanel();
		showWindow();
	}

	private void setFrame() {
		mainFrame = new JFrame("I'm hungry, I want more!");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void setTabPane() {
		tabs = new JTabbedPane();
		mainFrame.add(tabs);
	}
	
	private void setOrderPanel() {
		orderPanel = new JPanel();
		orderPanel.setLayout(new GridLayout(4, 1));
		
		SpinnerModel burgerModel = new SpinnerNumberModel(0, 0, 10, 1);
		SpinnerModel lasagnaModel = new SpinnerNumberModel(0, 0, 10, 1);
		SpinnerModel sodaModel = new SpinnerNumberModel(0, 0, 10, 1);
		SpinnerModel coffeeModel = new SpinnerNumberModel(0, 0, 10, 1);
		SpinnerModel iceCreamModel = new SpinnerNumberModel(0, 0, 10, 1);
		SpinnerModel cakeModel = new SpinnerNumberModel(0, 0, 10, 1);
		
		String[] paymentStrings = { "Cash", "Credit Card" };
		
		JPanel clientPanel = new JPanel();
		
		JComboBox paymentOptions = new JComboBox(paymentStrings);
		clientPanel.add(paymentOptions);
		
		JLabel clientLabel = new JLabel("Client");
		clientPanel.add(clientLabel);
		JTextField clientField = new JTextField(15);
		clientPanel.add(clientField);
		
		clientLabel.setLabelFor(clientField);
		
		orderPanel.add(clientPanel);
		
		JPanel spinnerPanel = new JPanel();
		spinnerPanel.setLayout(new GridLayout(3, 2));
		
		JLabel burgerCheck = new JLabel("Burger", SwingConstants.CENTER);
		spinnerPanel.add(burgerCheck);
		JSpinner burgerSpinner = new JSpinner(burgerModel);
		spinnerPanel.add(burgerSpinner);
		
		JLabel lasagnaCheck = new JLabel("Lasagna", SwingConstants.CENTER);
		spinnerPanel.add(lasagnaCheck);
		JSpinner lasagnaSpinner = new JSpinner(lasagnaModel);
		spinnerPanel.add(lasagnaSpinner);
		
		JLabel sodaCheck = new JLabel("Soda", SwingConstants.CENTER);
		spinnerPanel.add(sodaCheck);
		JSpinner sodaSpinner = new JSpinner(sodaModel);
		spinnerPanel.add(sodaSpinner);
		
		JLabel coffeeCheck = new JLabel("Coffee (Capsule)", SwingConstants.CENTER);
		spinnerPanel.add(coffeeCheck);
		JSpinner coffeeSpinner = new JSpinner(coffeeModel);
		spinnerPanel.add(coffeeSpinner);
		
		JLabel iceCreamCheck = new JLabel("Ice Cream", SwingConstants.CENTER);
		spinnerPanel.add(iceCreamCheck);
		JSpinner iceCreamSpinner = new JSpinner(iceCreamModel);
		spinnerPanel.add(iceCreamSpinner);
		
		JLabel cakeCheck = new JLabel("Chocolate Cake", SwingConstants.CENTER);
		spinnerPanel.add(cakeCheck);
		JSpinner cakeSpinner = new JSpinner(cakeModel);
		spinnerPanel.add(cakeSpinner);
		
		orderPanel.add(spinnerPanel);
		
		JPanel observationsPanel = new JPanel();
		
		JLabel observationsLabel = new JLabel("Observations");
		observationsPanel.add(observationsLabel);
		JTextField observationsField = new JTextField(20);
		observationsLabel.setLabelFor(observationsField);
		observationsPanel.add(observationsField);
		
		JLabel subtotalLabel = new JLabel("Subtotal: 0.0");
		observationsPanel.add(subtotalLabel);
		
		orderPanel.add(observationsPanel);
		
		JPanel buttonsPanel = new JPanel();
		
		JButton subtotalButton = new JButton("Calculate Subtotal");
		subtotalButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int burgersNumber = (Integer) burgerSpinner.getValue();
				double burgerSubtotal = burgersNumber * burger.getPrice();
				
				int lasagnaNumber = (Integer) lasagnaSpinner.getValue();
				double lasagnaSubtotal = lasagnaNumber * lasagna.getPrice();
				
				int sodasNumber = (Integer) sodaSpinner.getValue();
				double sodaSubtotal = sodasNumber * soda.getPrice();
				
				int coffeeNumber = (Integer) coffeeSpinner.getValue();
				double coffeeSubtotal = coffeeNumber * coffee.getPrice();
				
				int sundaesNumber = (Integer) iceCreamSpinner.getValue();
				double sundaeSubtotal = sundaesNumber * iceCream.getPrice();
				
				int cakeNumber = (Integer) cakeSpinner.getValue();
				double cakeSubtotal = cakeNumber * chocoCake.getPrice();
				
				double subtotal = burgerSubtotal + sodaSubtotal + sundaeSubtotal + 
									coffeeSubtotal + lasagnaSubtotal + cakeSubtotal;
				
				subtotalLabel.setText("Subtotal: " + String.valueOf(subtotal));
			}
		});
		buttonsPanel.add(subtotalButton);
		
		JButton orderButton = new JButton("Order");
		orderButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int confirm = JOptionPane.showConfirmDialog(null, 
				"Complete Order?", "Confirm", JOptionPane.YES_NO_OPTION, 
				JOptionPane.QUESTION_MESSAGE);
				
				if (confirm == JOptionPane.YES_OPTION) {
					
					String clientName = clientField.getText();
					
					if (clientName.equals("")) {
						JOptionPane.showMessageDialog(mainFrame, "Please insert Client Name!");
						return;
					}
					
					int burgersNumber = (Integer) burgerSpinner.getValue();
					int lasagnasNumber = (Integer) lasagnaSpinner.getValue();
					int sodasNumber = (Integer) sodaSpinner.getValue();
					int coffeesNumber = (Integer) coffeeSpinner.getValue();
					int sundaesNumber = (Integer) iceCreamSpinner.getValue();
					int cakesNumber = (Integer) cakeSpinner.getValue();
										
					String observations = observationsField.getText();
					
					List<OrderedProduct> orderedProducts = new ArrayList<>();
					
					if (burgersNumber != 0) {
						if (burger.getStock().isStockLow(burgersNumber)) {
							JOptionPane.showMessageDialog(mainFrame, "Burger Stock Low!");
							return;
						}
						else {
							OrderedProduct orderedBurgers = new OrderedProduct(burger, burgersNumber);
							orderedProducts.add(orderedBurgers);
						}
					}
					
					if (lasagnasNumber != 0) {
						if (lasagna.getStock().isStockLow(lasagnasNumber)) {
							JOptionPane.showMessageDialog(mainFrame, "Lasagna Stock Low!");
							return;
						}
						else {
							OrderedProduct orderedLasagnas = new OrderedProduct(lasagna, lasagnasNumber);
							orderedProducts.add(orderedLasagnas);
						}
					}
					
					if (sodasNumber != 0) {
						if (soda.getStock().isStockLow(sodasNumber)) {
							JOptionPane.showMessageDialog(mainFrame, "Soda Stock Low!");
							return;
						}
						else {
							OrderedProduct orderedSodas = new OrderedProduct(soda, sodasNumber);
							orderedProducts.add(orderedSodas);
						}
					}
					
					if (coffeesNumber != 0) {
						if (coffee.getStock().isStockLow(coffeesNumber)) {
							JOptionPane.showMessageDialog(mainFrame, "Coffee Stock Low!");
							return;
						}
						else {
							OrderedProduct orderedCoffees = new OrderedProduct(coffee, coffeesNumber);
							orderedProducts.add(orderedCoffees);
						}
					}
					
					if (sundaesNumber != 0) {
						if (iceCream.getStock().isStockLow(sundaesNumber)) {
							JOptionPane.showMessageDialog(mainFrame, "Burger Stock Low!");
							return;
						}
						else {
							OrderedProduct orderedSundaes = new OrderedProduct(iceCream, sundaesNumber);
							orderedProducts.add(orderedSundaes);
						}
					}
					
					if (cakesNumber != 0) {
						if (chocoCake.getStock().isStockLow(cakesNumber)) {
							JOptionPane.showMessageDialog(mainFrame, "Cake Stock Low!");
							return;
						}
						else {
							OrderedProduct orderedCakes = new OrderedProduct(chocoCake, cakesNumber);
							orderedProducts.add(orderedCakes);
						}
					}
					
					Order order = new Order(clientName, orderedProducts, observations);
					double change = 0.0;
					double payment = 0.0;
					
					if ("Cash".equals(paymentOptions.getSelectedItem().toString())) {
						while (payment < order.getValue()) {
							try {
								payment = Double.parseDouble(JOptionPane.showInputDialog(mainFrame, "Cash Given:"));
							} catch(NumberFormatException numEx) {
								JOptionPane.showMessageDialog(mainFrame, "Invalid input!");
							} catch(NullPointerException nullEx) {
								return;
							}
							if (payment >= order.getValue()) {
								CashPayment cash = new CashPayment(payment);
								cash.pay(order);
								change = cash.getChange();
							} 
							else {
								JOptionPane.showMessageDialog(mainFrame, "Insuficient Amount!");
							}
						}
					} 
					
					else {
						new CreditCardPayment().pay(order);
					}
					 
					JLabel client = new JLabel("Client: " + clientName);
					JLabel bill = new JLabel("Total: " + String.valueOf(order.getValue()));
					JLabel changeLabel = new JLabel("Change: " + String.valueOf(change));
					JLabel observationsLabel = new JLabel("Obs: " + observations);
					
					final JComponent[] inputs = new JComponent[] {
						client, bill, changeLabel, observationsLabel
					};
					
					JOptionPane.showMessageDialog(mainFrame, inputs, "Order Resume", JOptionPane.PLAIN_MESSAGE);
				}
			}
		});
		buttonsPanel.add(orderButton);
		
		orderPanel.add(buttonsPanel);
		
		tabs.addTab("New Order", orderPanel);
	}
	
	private void setStockPanel() {
		
		String[] columnNames = {"Item", "Price", "Amount in Stock"};
		Object[][] data = {
			{burger.getName(), burger.getPrice(), burger.getStock().getAmount()},
			{lasagna.getName(), lasagna.getPrice(), lasagna.getStock().getAmount()},
			{soda.getName(), soda.getPrice(), soda.getStock().getAmount()},
			{coffee.getName(), coffee.getPrice(), coffee.getStock().getAmount()},
			{iceCream.getName(), iceCream.getPrice(), iceCream.getStock().getAmount()},
			{chocoCake.getName(), chocoCake.getPrice(), chocoCake.getStock().getAmount()}
		};
		
		DefaultTableModel stockTableModel = new DefaultTableModel(data, columnNames);
		
		stockTable = new JTable(stockTableModel);
		stockTable.setEnabled(false);
		stockTablePane = new JScrollPane(stockTable);
		stockPanel = new JPanel();
		
		JButton updateButton = new JButton("Update");
		updateButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String[] columnNames = {"Item", "Price", "Amount in Stock"};
				Object[][] data = {
					{burger.getName(), burger.getPrice(), burger.getStock().getAmount()},
					{lasagna.getName(), lasagna.getPrice(), lasagna.getStock().getAmount()},
					{soda.getName(), soda.getPrice(), soda.getStock().getAmount()},
					{coffee.getName(), coffee.getPrice(), coffee.getStock().getAmount()},
					{iceCream.getName(), iceCream.getPrice(), iceCream.getStock().getAmount()},
					{chocoCake.getName(), chocoCake.getPrice(), chocoCake.getStock().getAmount()}
				};

				DefaultTableModel stockTableModel = new DefaultTableModel(data, columnNames);
				
				stockTable.setModel(stockTableModel);
			}			
		});
		
		stockPanel.add(stockTablePane);
		stockPanel.add(updateButton);
		tabs.addTab("Stock", stockPanel);
	}
	
	private void showWindow() {
		mainFrame.pack();
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
	}
	
}
