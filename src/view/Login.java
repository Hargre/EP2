package view;

import controllers.LoginController;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author felipe
 */
public final class Login {
	private JFrame loginFrame;
	private JPanel headerPanel;
	private JPanel inputPanel;
	private JPanel buttonsPanel;
	private JTextField userText;
	private JPasswordField passwordText;
	private JButton loginButton;
	private JButton registerButton;
	
	public Login() {
		setWindow();
	}
	
	private void setWindow() {
		setFrame();
		setHeaderPanel();
		setInputPanel();
		setButtonsPanel();
		setLoginButton();
		setRegisterButton();
		showWindow();
	}

	private void setFrame() {
		loginFrame = new JFrame("I'm hungry, I want more!");
		loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void setHeaderPanel() {
		headerPanel = new JPanel();
		
		JLabel headerLabel = new JLabel("Begin Session");
		headerPanel.add(headerLabel);
		
		loginFrame.add(headerPanel);
	}

	private void setInputPanel() {
		inputPanel = new JPanel();
		inputPanel.setLayout(new GridLayout(2, 2));
		
		JLabel userLabel = new JLabel("Username:");
		inputPanel.add(userLabel);
		
		userText = new JTextField(10);
		inputPanel.add(userText);
		
		JLabel passwordLabel = new JLabel("Password:");
		inputPanel.add(passwordLabel);
		
		passwordText = new JPasswordField(10);
		inputPanel.add(passwordText);
		
		loginFrame.add(inputPanel);
	}

	private void setButtonsPanel() {
		buttonsPanel = new JPanel();
		loginFrame.add(buttonsPanel);
	}

	private void setLoginButton() {
		loginButton = new JButton("Login");
		loginButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String username = userText.getText();
				String password = String.valueOf(passwordText.getPassword());
				
				if (LoginController.authenticateLogin(username, password)) {
					loginFrame.dispose();
					MainApp mainApp = new MainApp();
				} 
				else {
					JOptionPane.showMessageDialog(null, "Invalid Username or Password!");
				}
			}
		});
		
		buttonsPanel.add(loginButton);
	}

	private void setRegisterButton() {
		registerButton = new JButton("Register");
		registerButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				loginFrame.dispose();
				Register register = new Register();
			}
		});
				
		buttonsPanel.add(registerButton);
	}

	private void showWindow() {
		loginFrame.pack();
		loginFrame.setLocationRelativeTo(null);
		loginFrame.setLayout(new FlowLayout());
		loginFrame.setSize(300, 160);
		loginFrame.setVisible(true);
	}
	
	public static void main(String[] args) {
		Login login = new Login();
	}
}
