# EP2 - OO (UnB Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, usando a tecnologia Java Swing.

## Para compilar/executar (Netbeans):

* Abrir a IDE Netbeans;
* Abrir o projeto;
* Apertar o botão "Run" para executar.

## Instruções de uso:

* Para entrar no sistema, pode-se criar um usuário novo, selecionando a opção "Register", ou usar um dos usuários já salvos no sistema.
* Para conferir as contas pré-existentes, abrir o arquivo employeeInfo.txt, na pasta raiz do projeto.
* Dentro desse arquivo, os dados estão organizados na forma userName:password.
* Após fazer o log-in, podem ser realizados pedidos na aba New Order e conferir o estoque na aba Stock. 

## Instruções de testes:

* Com o projeto aberto no Netbeans, executar a instrução "Run Tests" na aba "Run" do menu. 
* Os testes unitários serão realizados automaticamente e seu resultado exibido no console da IDE.
