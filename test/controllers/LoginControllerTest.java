/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author felipe
 */
public class LoginControllerTest {

	/**
	 * Test of authenticateLogin method, of class LoginController.
	 */
	@Test
	public void testAuthenticateLogin() {
		String username = "test";
		String password = "testpwd";
		
		String fakeUser = "fake";
		String fakePassword = "fakepwd";
		
		assertTrue(LoginController.authenticateLogin(username, password));
		assertFalse(LoginController.authenticateLogin(fakeUser, fakePassword));
	}
	
}
