/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author felipe
 */
public class StockTest {
	
	public StockTest() {
	}

	/**
	 * Test of isStockLow method, of class Stock.
	 */
	@Test
	public void testIsStockLow() {
		Stock testStock = new Stock(10);
		int amountOrdered = 7;
		
		assertTrue(testStock.isStockLow(amountOrdered));
	}
	
}
