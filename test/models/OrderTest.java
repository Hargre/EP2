/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author felipe
 */
public class OrderTest {
	
	@Test
	public void addingProductsToOrder() {
		Product burger = new Product("Burger", 15, 20);
		Product coke = new Product("Coke", 5, 20);
		
		OrderedProduct orderBurger = new OrderedProduct(burger, 1);
		OrderedProduct orderSoda = new OrderedProduct(coke, 1);
		
		List<OrderedProduct> products = new ArrayList<>();
		products.add(orderBurger);
		products.add(orderSoda);
		
		Order testOrder = new Order("Felipe", products, "no pickles");
		
		assertEquals(20.0, testOrder.getValue(), 0.0001);
		assertEquals(testOrder.client.getName(), "Felipe");
		assertEquals(19, burger.getStock().getAmount());
	}
	
}
