/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author felipe
 */
public class OrderedProductTest {
	
	public OrderedProductTest() {
	}

	@Test
	public void testGetProductFromStock() {
		OrderedProduct testOrdered = new OrderedProduct(new Product("Test", 10.0, 10), 3);
		assertEquals(30.0, testOrdered.getPrice(), 0.00001);
	}
	
}
