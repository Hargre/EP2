/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author felipe
 */
public class ProductTest {
	@Test
	public void testItemsInStock() {
		Product testProduct = new Product("Test Food", 20.0, 30);
		assertEquals(30, testProduct.getStock().getAmount());
	}
	
	@Test
	public void testGetItems() {
		System.out.println("getItems");
		int amount = 10;
		Product instance = new Product("Test Food", 20.0, 30);
		instance.getItems(amount);
		
		assertEquals(20, instance.getStock().getAmount());
	}
	
}
